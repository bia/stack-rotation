package plugins.spop.rotation3D;

import icy.image.IcyBufferedImage;
import icy.sequence.Sequence;
import icy.type.collection.array.Array1DUtil;
import icy.util.OMEUtil;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzVarSequence;
import plugins.adufour.ezplug.EzVarText;

public class StackRotation extends EzPlug implements Block
{
    
    EzVarSequence input  = new EzVarSequence("Input");
    EzVarText     type   = new EzVarText("Type", new String[] { "FrontToRight", "FrontToTop" }, false);
    // VarSequence output = new VarSequence("Output", false);
    
    EzVarSequence output = new EzVarSequence("Output");
    
    @Override
    public void initialize()
    {
        // TODO Auto-generated method stub
        super.addEzComponent(input);
        super.addEzComponent(type);
        // super.addOutput(output);
        super.setTimeDisplay(true);
        
    }
    
    @Override
    public void execute()
    {
        
        Sequence sequence = input.getValue(true);
        Sequence seq_out = new Sequence(OMEUtil.createOMEXMLMetadata(sequence.getOMEXMLMetadata()));
        if (type.getValue().equalsIgnoreCase("FrontToRight"))
        {
            seq_out = FrontToRight(sequence);
            seq_out.setName("FrontToRight");
        }
        if (type.getValue().equalsIgnoreCase("FrontToTop"))
        {
            seq_out = FrontToTop(sequence);
            seq_out.setName("FrontToTop");
            
        }
        
        if (getUI() != null) addSequence(seq_out);
        output.setValue(seq_out);
        
    }
    
    public static Sequence FrontToRight(Sequence sequence)
    {
        
        int z = 0;
        
        Sequence seq = new Sequence();
        
        int dim_x = sequence.getSizeX();
        int dim_y = sequence.getSizeY();
        int dim_z = sequence.getSizeZ();
        int dim_t = sequence.getSizeT();
        int dim_c = sequence.getSizeC();
        double[][] tab = new double[dim_z][dim_x * dim_y];
        double[][][] tab_r = new double[dim_c][dim_x][dim_y * dim_z];
        
        for (int t = 0; t < dim_t; t++)
        {
            for (int c = 0; c < dim_c; c++)
            {
                for (z = 0; z < dim_z; z++)
                {
                    tab[z] = Array1DUtil.arrayToDoubleArray(sequence.getDataXY(t, z, c), false);
                }
                for (z = 0; z < dim_z; z++)
                    for (int y = 0; y < dim_y; y++)
                        for (int x = 0; x < dim_x; x++)
                        {
                            tab_r[c][x][y * dim_z + z] = tab[z][y * dim_x + x];
                        }
            }
            
            for (z = 0; z < dim_x; z++)
            {
                IcyBufferedImage Icy = new IcyBufferedImage(dim_z, dim_y, dim_c, sequence.getDataType());
                for (int c = 0; c < dim_c; c++)
                    Icy.setDataXY(c, tab_r[c][z]);
                seq.setImage(t, z, Icy);
            }
        }
        
        System.gc();
        return seq;
        
    }
    
    public static Sequence FrontToTop(Sequence sequence)
    {
        
        int z = 0;
        
        Sequence seq = new Sequence();
        
        int dim_x = sequence.getSizeX();
        int dim_y = sequence.getSizeY();
        int dim_z = sequence.getSizeZ();
        int dim_t = sequence.getSizeT();
        int dim_c = sequence.getSizeC();
        double[][] tab = new double[dim_z][dim_x * dim_y];
        double[][][] tab_r = new double[dim_c][dim_y][dim_x * dim_z];
        
        for (int t = 0; t < dim_t; t++)
        {
            for (int c = 0; c < dim_c; c++)
            {
                for (z = 0; z < dim_z; z++)
                {
                    tab[z] = Array1DUtil.arrayToDoubleArray(sequence.getDataXY(t, z, c), false);
                }
                for (z = 0; z < dim_z; z++)
                    for (int y = 0; y < dim_y; y++)
                        for (int x = 0; x < dim_x; x++)
                        {
                            tab_r[c][y][z * dim_x + x] = tab[z][y * dim_x + x];
                        }
            }
            
            for (z = 0; z < dim_y; z++)
            {
                IcyBufferedImage Icy = new IcyBufferedImage(dim_x, dim_z, dim_c, sequence.getDataType());
                for (int c = 0; c < dim_c; c++)
                    Icy.setDataXY(c, tab_r[c][z]);
                seq.setImage(t, z, Icy);
            }
        }
        
        System.gc();
        
        return seq;
        
    }
    
    public void clean()
    {
    }
    
    @Override
    public void declareInput(VarList inputMap)
    {
        // TODO Auto-generated method stub
        inputMap.add(input.getVariable());
        inputMap.add(type.getVariable());
        
    }
    
    @Override
    public void declareOutput(VarList outputMap)
    {
        // TODO Auto-generated method stub
        outputMap.add(output.getVariable());
        
    }
    
}
